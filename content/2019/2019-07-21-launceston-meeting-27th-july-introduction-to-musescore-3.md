Title: Launceston July Meeting - An introduction to MuseScore 3
Date: 2019-07-19
Category: Launceston

Hi people. For this month's Launceston meeting, we will be taking a look at how to write and play music with the F/OSS composition and notation software [MuseScore](https://musescore.org/).

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 27th July<br />
  2:00pm Start

As always, everybody is welcome to come along and join the discussion. Hope to see you there!
