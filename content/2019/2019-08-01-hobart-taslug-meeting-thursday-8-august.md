Title: Hobart TasLUG Meeting: Thursday 8 August
Date: 2019-08-01
Author: John Kristensen
Category: Hobart
Tags: meeting

The month has just begun but TasLUG is almost upon us. This month's meeting
will feature the following talks:

---

#### Steven Honson
An introduction to [WireGuard](https://www.wireguard.com/), a fast, modern,
cross-platform, secure VPN solution that is on a trajectory towards inclusion
in the mainline Linux kernel.

#### Chris Barnard
A short talk about his experience with Aurora's time-of-use tariff, which is a
follow-up to a similar talk he gave last year about TasNetworks.

#### General Discussion
There should be some time at the end of the meeting to open things up for
general discussion and for people to say a few words about any projects they
are currently working on.

---

The venue for this month will be Enterprize Hobart:

When:
: Thursday 8 August 2019 - 6:00pm for a 6:30pm start

Where:
: Enterprize - Hobart<br />
  5th floor, Hobart City Council Building<br />
  24 Davey Street ([map](https://goo.gl/maps/SNWs8FJcvEk))<br />

***Note:** the doors inside the foyer will be locked, so you will need to use
the intercom to let us know to buzz you in.*

Enterprize provide a selection of alcoholic/non-alcoholic drinks and coffee
pods for the appropriate donation, as well as free instant coffee and tea. So
arrive at 6:00pm, get a drink, and have a bit of a mingle before the talks
start at 6:30pm. We will also be heading out to find somewhere to eat
afterwards for those interested.

We would like to to extend a big thank you to
[Enterprize](https://www.enterprize.space/) for generously allowing us to use
their space to hold regular meetings.
