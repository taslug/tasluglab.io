Title: Hobart TasLUG Meeting: Thursday 11 April
Date: 2019-04-04
Author: John Kristensen
Category: Hobart
Tags: meeting

April is upon us and with it brings an [earlier than
usual]({filename}./2019-03-29-hobart-meetings-changing-weeks.md) TasLUG
meeting, the second of the year to feature talks:

---

#### Ben Short
[Grafana](https://grafana.com/) is a data visualisation and analytics platform
and the project has recently started developing a logging platform called
[Loki](https://grafana.com/loki). Ben will take you through setting up some
basic network & syslog monitoring using Grafana, Loki, and
[Docker](https://www.docker.com/) containers.

---

The venue for this month will be Enterprize Hobart:

When:
: Thursday 11 April 2019 - 6:00pm for a 6:30pm start

Where:
: Enterprize - Hobart<br />
  5th floor, Hobart City Council Building<br />
  24 Davey Street ([map](https://goo.gl/maps/SNWs8FJcvEk))<br />

***Note:** the doors inside the foyer will be locked, so you will need to use the
intercom to let us know to buzz you in. However, if you arrive after 6:30pm,
then we'll leave a note with a phone number to call as the intercom isn't near
where the talks will be.*

Enterprize provide a selection of alcoholic/non-alcoholic drinks and coffee
pods for the appropriate donation, as well as free instant coffee and tea. So
come at 6:00pm, get a drink, and have a bit of a mingle before the talks start
at 6:30pm. We will also be heading out to find somewhere to eat afterwards for
those interested.

I would like to to extend a big thank you to
[Enterprize](https://www.enterprize.space/) for generously allowing
us to use their space to hold regular meetings. 
