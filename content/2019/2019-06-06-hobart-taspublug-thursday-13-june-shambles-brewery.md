Title: Hobart TasPubLUG: Thursday 13 June - Shambles Brewery
Date: 2019-06-06
Author: John Kristensen
Category: Hobart

Unfortunately the room at Enterprize Hobart is already booked out for next
Thursday, so there won't be a meeting with talks this month as previously
planned. However, Julius has volunteered to host the TasLUG meeting while John
is away next month, so we will just switch the months around and have talks
next month instead.

This month we will instead be having a TasPubLUG at the Shambles Brewery


When:
: 6:30pm, Thursday 9 May 2019

Where:
: Shambles Brewery (222 Elizabeth Street Hobart)<br/>
  [http://www.shamblesbrewery.com.au/](http://www.shamblesbrewery.com.au/)


If you are planning to come along (even for a quick drink) it would be great if
you can let [John](mailto:jkristen@theintraweb.net) know. If a largish number
of people plan to come along then I'll reserve a table.
