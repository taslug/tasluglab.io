Title: Hobart end of year TasLUG dinner: Thursday 14 November
Date: 2019-11-07
Category: Hobart

The end of the year is not yet quite upon us, but November will be the last
TasLUG meeting of the year, so we will be catching up over an end of year
dinner.

When:
: 6:30pm, Thursday 14 November

Where:
: Amigos Mexican Restaurant<br />
  [http://amigosmexicanhobart.com.au/](http://amigosmexicanhobart.com.au/)

If you plan on coming along, then please **RSVP to
[John](mailto:jkristen@theintraweb.net) by Tuesday 12 November** so he can make
the appropriate booking.

If Amigos are unable to fit us in, the backup option will be Mother India just down the road.
