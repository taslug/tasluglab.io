Title: Hobart TasLUG Meeting: Thursday 11 July 2019
Date: 2019-07-11
Author: Julius Roberts
Category: Hobart
Tags: meeting

July is here and with John away I'm attempting to fill his rather large shoes.  This month we're on again and have the following items organized:

---

#### Peter Billam
audio2midi: convert features of audio signals into MIDI data

#### Karl Goetz
Debian 10 ("Buster") release party!!  Including something "celebratory" to eat :)

#### Your name here?
Lightening talks.  A technology you're excited about??  Share it with us (un-timed).

---

The venue for this month will be Enterprize Hobart:

When:
: Thursday 11 July 2019 - 6:00pm for a 6:30pm start

Where:
: Enterprize - Hobart<br />
  5th floor, Hobart City Council Building<br />
  24 Davey Street ([map](https://goo.gl/maps/SNWs8FJcvEk))<br />

***Note:** the doors inside the foyer will be locked, so you will need to use the
intercom to let us know to buzz you in. However, if you arrive after 6:30pm,
then we'll leave a note with a phone number to call as the intercom isn't near
where the talks will be.*

Enterprize provide a selection of alcoholic/non-alcoholic drinks and coffee
pods for the appropriate donation, as well as free instant coffee and tea. So
come at 6:00pm, get a drink, and have a bit of a mingle before the talks start
at 6:30pm.
