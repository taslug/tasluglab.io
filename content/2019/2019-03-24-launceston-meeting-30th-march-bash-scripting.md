Title: Launceston Feburary Meeting - Bash Scripting for Everyone
Date: 2019-03-24
Category: Launceston

Hi people! For this month's Launceston meeting, we'll be taking a look at Bash shell scripting. It doesn't matter swhether you're an old hand or a complete noob, there should be something in this talk for everyone. We'll be starting with simple abstract scripts to demonstrate techniques, and moving up to useful practical examples for everyday use.

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 30th March<br />
  2:00pm Start

As always, everybody is welcome to come along and join the discussion. Hope to see you there!
