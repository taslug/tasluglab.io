Title: Launceston April Meeting - How to build gadgets with a Raspberry Pi and Nerves
Date: 2019-04-12
Category: Launceston

Hi people! For this month's Launceston meeting, Elliott will be discussing and demonstrating how to use [Nerves](https://nerves-project.org/) to drive Raspberry Pis for custom hardware projects.

Want to know what Elixir and Nerves are, and why you would want to use them to make robots? Find the answers and a walkthrough of creating fresh project at this month's meeting!

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 27th April<br />
  2:00pm Start

As always, everybody is welcome to come along and join the discussion. Hope to see you there!
