Title: Meetings
SortOrder: 20

## Launceston
Northern TasLUG members meet on the last Saturday of the month at
[Enterprize](https://www.enterprize.space/) (Launceston) co-working space.

**Enterprize (Launceston)**<br />
22-24 Paterson Street,<br />
Launceston, Tas

Meetings start from **2:00pm** and are coordinated by
[Josh Bush](mailto:cheeseness@freesoftwareforall.org).

---

## Hobart
### Talks
Southern TasLUG members meet on the second Thursday of (roughly) every second
month at [Enterprize](https://www.enterprize.space/) (Hobart).

**Enterprize (Hobart)**<br />
5th floor, Hobart City Council Building<br />
24 Davey Street<br />
Hobart, Tas

Meetings are at **6:00pm** for a 6:30pm start and coordinated by
[John Kristensen](mailto:jkristen@theintraweb.net).

### PubLUGs
For the alternating months when no talks are scheduled members head to a pub
for something to eat and/or drink and a casual catchup.

The venue changes depending on suggestions from TasLUG members, so keep an eye
on the [mailing list]({filename}./mailing-list.md) or website for
announcements.

---

## Thanks
TasLUG members are very grateful to [Enterprize](https://www.enterprize.space/)
(Hobart & Launceston) for allowing TasLUG meetings to be held in their space.
