Title: Hobart TasLUG Meeting: Thursday 13 February
Date: 2020-02-06
Author: John Kristensen
Category: Hobart
Tags: meeting

Hobart TasLUG meetings are back for another year. This month's meeting will
feature the following talk<s>s</s>:

---

#### Karl Goetz
[Tendenci](https://www.tendenci.com/) - The Open Source Association Management System (AMS)


<s>
#### John Kristensen
An overview of [OpenSSH](https://www.openssh.com/) features - including using
port forwarding and escape characters.
</s>

_(Update: unfortunately John's talk has been postponed until a later meeting)_

#### General Discussion
There should be some time at the end of the meeting to open things up for
general discussion and for people to say a few words about any projects they
are currently working on.

---

The venue for this month will be Enterprize Hobart:

When:
: Thursday 13 February 2020 - 6:00pm for a 6:30pm start

Where:
: Enterprize - Hobart<br />
  5th floor, Hobart City Council Building<br />
  24 Davey Street ([map](https://goo.gl/maps/SNWs8FJcvEk))<br />

***Note:** the doors inside the foyer will be locked, so you will need to use
the intercom to let us know to buzz you in.*

Enterprize provide a selection of alcoholic/non-alcoholic drinks and coffee
pods for the appropriate donation, as well as free instant coffee and tea. So
arrive at 6:00pm, get a drink, and have a bit of a mingle before the talks
start at 6:30pm. We will also be heading out to find somewhere to eat
afterwards for those interested.

We would like to to extend a big thank you to
[Enterprize](https://www.enterprize.space/) for generously allowing us to use
their space to hold regular meetings.
