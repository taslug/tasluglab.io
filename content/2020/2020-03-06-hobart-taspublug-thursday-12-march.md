Title: Hobart TasPubLUG: Thursday 12 March - In The Hanging Garden
Date: 2020-03-06
Author: John Kristensen
Category: Hobart

Next week is TasPubLUG time where we head to the pub (or other type of eatery)
to talk about Free and Open Source software, hardware and culture... but
usually just to generally hang out and have something to eat and drink.

When:
: 6:30pm, Thursday 12 March 2020

Where:
: [In The Hanging Garden](https://www.inthehanginggarden.com.au/) (112 Murray St, Hobart)

Please note that the venue is cashless, so you will need to bring your virtual
money device<sup>1</sup> with you... or just ask someone to make a purchase on
your behalf.

In The Hanging Garden is not really an all weather venue, so while the
[forecast](http://www.bom.gov.au/tas/forecasts/hobart.shtml) currently looks
good for next Thursday, the backup venue will be [The
Standard](https://www.standardburgers-online.com.au/) (177 Liverpool St,
Hobart) just down the road.

---

<sup>1</sup> usually a credit/debit card 
