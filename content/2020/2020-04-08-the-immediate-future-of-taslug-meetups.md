Title: The Immediate Future of TasLUG Meetings
Date: 2020-04-08
Author: John Kristensen
Category: Hobart

Later this week we would usually be holding one of our regular TasLUG meetings,
but given current circumstances with the COVID-19 pandemic we are no longer
able to meet up in person. It is unclear how long this situation will continue,
which means that there will be no TasLUG meetings for the foreseeable future.

However, it is import that we continue to maintain our social connections, and
we are very fortunate that TasLUG has an active on-line community. Many TasLUG
regulars can often been found hanging out in the Matrix chat channel, and there
has been some talk about trying to hold video conferences in lieu of in person
meet-ups.

During this time of increased social isolation I would encourage everyone to
join us in our friendly and welcoming [Matrix]({filename}../pages/chat.md)
channel (or [IRC]({filename}../pages/chat.md) if you prefer) and/or utilise the
[mailing list]({filename}../pages/mailing-list.md) to keep in touch. If you
need any help joining the chat channel then please don't hesitate to get in
contact with [me](mailto:jkristen@theintraweb.net) for a helping hand.

We are all looking forward to the day when we can once again meet-up in person,
but until then I hope everyone and their family stays safe and healthy.
