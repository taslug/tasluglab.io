Title: Hobart coordinator needed
Date: 2017-11-23
Category: Hobart

The current coordinator of the Hobart meetings recently posted the following on
the TasLUG [mailing list]({filename}../pages/mailing-list.md):

> Hi everyone,
> 
> This year has gotten very busy for me with many side projects and it doesn't
> look like it will let up any time soon. With declining attendance, I don't
> have the time to continue organising Hobart TasLUG meetings and I am open to
> someone else taking up the reins.
> 
> If you'd like to give it a go, let the list know in reply and/or join the
> chat on Matrix #taslug:matrix.org or IRC #taslug@freenode.
> 
> Cheers Scott

We would love to hear from you if you are interesting in helping to coordinate
future Hobart TasLUG meetings.
