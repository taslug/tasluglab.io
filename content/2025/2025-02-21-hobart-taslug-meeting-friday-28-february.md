Title: Hobart TasLUG Meeting: Friday 28 February
Date: 2025-02-21
Author: John Kristensen
Category: Hobart

After a long hiatus, TasLUG meetings are making a come-back!

For the first meeting back Karl Goetz and Monica Wood will be providing
overviews/summaries of a few of the talks presented at [Everything Open
2025](https://2025.everythingopen.au/).

----

#### Karl Goetz
  - [Detecting Csomic Ray Muons](https://2025.everythingopen.au/schedule/presentation/65/)
    (By Robert Hart)
  - [A Big Live Demo of Kanidm](https://2025.everythingopen.au/schedule/presentation/57/)
    (By William Brown)
  - [P3.express: A libre, minimalist project management method](https://2025.everythingopen.au/schedule/presentation/60/)
    (By Nader K. Rad)

#### Monica Wood
  - [Sustaining Open Source Software](https://2025.everythingopen.au/schedule/presentation/122/)
    (By Justin Warren - Day 1 Keynote)
  - [The Token Wars: Why not everything should be
    open](https://2025.everythingopen.au/schedule/presentation/59/)
    (By Kathy Reid)
---

The venue for this month will be GoodSoil Co-Working:

When:
: Friday 28 February 2025 - 5:00pm<br />
  (talks start at 6:00pm and will go for about an hour)

Where:
: GoodSoil Co-Working<br />
  29 John Street, Kingston
  ([map](https://umap.osm.ch/en/map/taslug-meeting_9352))<br />

What:
: Eat some pizza, listen to some talks, and finish with a general chat

Cost:
: $10 to cover food and drink.<br />
  [Medeopolis IT Solutions](https://medeopolis.com.au/) will be supporting the
  event by making up any shortfall in costs.

Please RSVP to Monica via the [mailing
list](https://lists.linux.org.au/pipermail/taslug/2025-February/000250.html)
([subscribe](https://lists.linux.org.au/mailman/listinfo/taslug)) or the [TasLUG
chat room]({filename}../pages/chat.md) so that appropriate cattering an be
arranged.

We would like to to extend a big thank you to Monica for her efforts in bring
TasLUG mettings back again.
