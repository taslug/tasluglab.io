Title: Hobart TasPubLUG: Thursday 18 October
Date: 2018-10-12
Category: Hobart
Tags: taspublug

This month is TasPubLUG time again. We will be heading to the pub to talk about
Free and Open Source software, hardware, and culture... or just to generally
hang out and having something to eat and drink. This time I've randomly pick
the Brunswick Hotel as the venue:

When:
: 6:30pm, Thursday 18 October

Where:
: Brunswick Hotel (67 Liverpool Street, Hobart)<br />
  [http://brunswickhotelhobart.com.au/](http://brunswickhotelhobart.com.au/)

If you are planning to come along (even for just a quick drink) please let John
know by Tuesday so the appropriate booking can be made if needed.
