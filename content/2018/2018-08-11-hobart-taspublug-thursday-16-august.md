Title: Hobart TasPubLUG: Thursday 16 August
Date: 2018-08-11
Category: Hobart
Tags: taspublug

As mentioned at the last Hobart meeting we will aim to have talks at
[Enterprize](https://www.enterprize.space/) every second month and have the LUG
at the pub the months in between. So this month is a pub month and someone in
the [chat channel]({filename}../pages/chat.md) suggested Tom McHugo's, so that's
where we will try.

When:
: 6:30pm, Thursday 16 August 2018

Where:
: Tom McHugo's (87 Macquarie Street, Hobart)

If you are planning to come along (even for a quick drink) please let John know
by Wednesday so we can make the appropriate booking if needed.
