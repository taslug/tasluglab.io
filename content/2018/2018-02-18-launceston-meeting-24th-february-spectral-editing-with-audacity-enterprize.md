Title: Launceston Meeting: 24th February - Spectral Editing With Audacity
Date: 2018-02-18 17:57 +1100
Category: Launceston

Hi people! For this month's Launceston meeting, we will look at the new(ish)
"spectral editing" tools in the F/OSS audio editing program Adacity, which
allow for a more visual way of analysing and editing unwanted sounds in audio
recordings.

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 24th February<br />
  2:00pm Start

As always, everybody is welcome to come along and join the discussion. Hope to
see you there!
