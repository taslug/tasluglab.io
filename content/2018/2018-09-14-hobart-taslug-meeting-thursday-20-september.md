Title: Hobart TasLUG Meeting: Thursday 20 September
Date: 2018-09-14
Category: Hobart
Tags: meeting

It is an odd month so that must mean it is a talk month at Hobart TasLUG. At
the meeting this month we will be having another two talks:

---

#### Julius Roberts
Using GitLabCI to build better software.

#### Peter Billam
Talking about some random-distribution generators in Lua and PostScript, some
Markov Chains, some first steps in torch - and then an unorthodox
Guitar-Tablature.

---

The venue for this month will be Enterprize Hobart:

When:
: Thursday 20 September 2018 - 6:00pm for a 6:30pm start

Where:
: Enterprize - Hobart<br />
  5th floor, Hobart City Council Building<br />
  24 Davey Street ([map](https://goo.gl/maps/SNWs8FJcvEk))<br />

**Note:** the doors inside the foyer will be locked, so you will need to use the
intercom to let us know to buzz you in. However, if you arrive after 6:30pm,
then we'll leave a note with a phone number to call as the intercom isn't near
where the talks will be.

Enterprize provide a selection of alcoholic/non-alcoholic drinks and coffee
pods for the appropriate donation, as well as free instant coffee and tea. So
come at 6:00pm, get a drink, and have a bit of a mingle before the talks start
at 6:30pm. We will also be heading out to find somewhere to eat afterwards for
those interested.

I would like to to extend a big thank you to
[Enterprize](https://www.enterprize.space/) for generously allowing
us to use their space to hold regular meetings. 
