Title: Hobart TasPubLUG: Thursday 19 April - Hope and Anchor Tavern
Date: 2018-04-11
Category: Hobart
Tags: taspublug

It is that time of the month again to meet up and talk about all things related
to free and open source software, hardware, and culture.

Unfortunately Shambles Brewery is a bit busy on the 3rd Thursday of the month
so we are trying a new location this month.

When:
: 6:30pm, Thursday 19 April 2018

Where:
: Hope and Anchor Tavern (65 Macquarie St, Hobart)

Let John know if you are planning to come along. If we hear from more than a
handful of people that they are coming then we'll book a table, otherwise we
will just rock up see what is available.

Looking forward to seeing everyone next week on Thursday.
