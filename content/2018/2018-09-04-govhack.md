Title: GovHack 2018
Date: 2018-09-04
Category: General
Tags: govhack

A quick not to let everyone know that
[Enterprize](https://www.enterprize.space/) are hosting this years GovHack. It
is on this weekend and you can find more details about the Hobart event on
their [Facebook page](https://www.facebook.com/events/443185192842253/).
