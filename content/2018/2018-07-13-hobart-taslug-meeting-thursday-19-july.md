Title: Hobart TasLUG Meeting: Thursday 19 July
Date: 2018-07-13
Category: Hobart
Tags: meeting

Talks at Hobart TasLUG are back! At the meeting this month we will be having
two short talks:

---

#### Tim Serong
Talking about how he got a Raspberry Pi to talk to his solar PV inverter to
monitory its electricity generation.

#### Chris Barnard
Talking about his participation in the TasNetwork
[emPOWERing You Trial](https://www.tasnetworks.com.au/customer-engagement/tariff-reform/empoweringyou/).

---

By happy coincidence both of these talks about electricity related topics will
be held at our new venue: [Enterprize](https://www.enterprize.space/) - which
just happens to be located in the old Hydro Electrical Commission building.


When:
: Thursday 19 July, 2018 - 6:00pm for a 6:30pm start

Where:
: Enterprize - Hobart<br />
  5th floor, Hobart City Council Building<br />
  24 Davey Street ([map](https://goo.gl/maps/SNWs8FJcvEk))

**Note:** the doors inside the foyer will be locked, so you will need to use
the intercom to let us know to buzz you in. However, if you arrive after
6:30pm, then we'll leave a note with a phone number to call as the intercom
isn't near where the talks will be.

Enterprize provide a selection of alcoholic/non-alcoholic drinks and coffee
pods for the appropriate donation, as well as free instant coffee and tea. So
come at 6:00pm, get a drink, and have a bit of a mingle before the talks start
at 6:30pm. We will also be heading out to find somewhere to eat afterwards for
those interested.

I would like to to extend a big thank you to Enterprize for generously allowing
us to use their space to hold regular meetings into the future.
