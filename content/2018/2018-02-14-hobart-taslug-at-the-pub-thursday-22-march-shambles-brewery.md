Title: Hobart TasLUG at the Pub: Thursday 22 February - Shambles Brewery
Date: 2018-02-18
Category: Hobart
Tags: taspublug

For those of you that missed the news, Scott stepped down as the Hobart
coordinator of TasLUG meetings at the end of last year. Volunteer work like
this can often be a thankless job, so in case it hasn't already been said...

We would like to say a huge thank you on behalf of the TasLUG community to
Scott for organising these meetings for the last 4 years. He has done a
wonderful job of finding speakers with interesting talks to present[^1] and
ensuring there was always a space to have the monthly meetings.

The upshot of this though is that no one else has yet put their hand up to
organise meetings so they are currently on hiatus for the foreseeable future.

However a few people have expressed an interest in still catching up regularly
in person. So this month we will be heading to the pub to talk about the usual
things we talk about at meetings.

When:
: 6:30pm, Thursday 22 March 2018

Where:
: Shambles Brewery (222 Elizabeth Street Hobart)
  [http://www.shamblesbrewery.com.au/](http://www.shamblesbrewery.com.au/)

There might be a bit of discussion about the future of TasLUG in Hobart, so if
you have any opinions or some free time to help out it would be great if you
can come along. If you can't make it, then we always enjoy people dropping by
[IRC/Matrix]({filename}../pages/chat.md) for a chat.

[^1]: Scott often ran tutorials or give talks himself to fill in the months
      when no other presenters were available.
