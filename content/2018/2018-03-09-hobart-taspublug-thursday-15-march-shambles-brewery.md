Title: Hobart TasPubLUG: Thursday 15 March - Shambles Brewery
Date: 2018-03-09
Category: Hobart
Tags: taspublug

The last Hobart TasPubLUG was only a few weeks ago, but we thought we would try
to get the Hobart meet-ups back to their regular 3rd Thursday of the month and
this month the 3rd Thursday is an early one... Oh well.

This month we will be heading to the pub again to talk about any topics related
to Free and Open Source software, hardware, and culture that take our fancy.

There was a special request for this month that people bring along any hardware
projects they are working on, or new gadgets they have recently acquired, and
give a bit of an explanation about what they are being used for. If you have a
new single board computer, dev board, crypto card, or yubi key, etc. bring it
along, we would be keen to hear about it.

When:
: 6:30pm, Thursday 22 March 2018

Where:
: Shambles Brewery (222 Elizabeth Street Hobart)<br />
  [http://www.shamblesbrewery.com.au/](http://www.shamblesbrewery.com.au/)

Bring:
: Any new hardware projects or gadgets.

We look forward to seeing everyone on Thursday.
