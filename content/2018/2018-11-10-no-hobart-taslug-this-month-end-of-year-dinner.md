Title: No Hobart TasLUG this month & end of year dinner
Date: 2018-11-10
Category: Hobart

There will be no Hobart TasLUG meeting this coming Thursday. The two speakers
we had lined up for this months meeting will both be out of the state this
Thursday and we haven't had any time to organise anyone to replace them.

However it is getting close to the end of the year and there has been some talk
about having some sort of end of year dinner. Our current thinking is that we
have this on Thursday 29 November so as to avoid the silly season that is
December.

A couple of suggestions for places to go for the dinner have been the Tacos or
Amigo's Mexican restaurants, but please get in touch with John if you have any
other suggestions or preferences. Firmer details will be posted closer to the
date.
