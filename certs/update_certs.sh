#!/bin/bash

BASE_DIR="$(cd $(dirname $0); pwd)"
BIN_DIR="$BASE_DIR/bin"

function _check_env_var
{
    var="$1"
    if [ -z "${!var}" ]; then
        echo "Error: missing '$var' environment variable"
        exit
    else
        export "$var"
    fi
}

# acme.sh paths
ACME_SH="$BIN_DIR/acme.sh"
export LE_WORKING_DIR="$BASE_DIR/.letsencrypt"

# CloudFlare credentials
_check_env_var CF_Email
_check_env_var CF_Key

# GitLab credentials
export GITLAB_PROJECT_ID="9221662"
_check_env_var GITLAB_TOKEN

# Issue new certificates
"$ACME_SH" --issue \
    --domain taslug.org.au \
    --domain www.taslug.org.au \
    --dns dns_cf \
    --dnssleep 10

# Upload new certificates to GitLab
export GITLAB_DOMAIN="taslug.org.au"
"$ACME_SH" --deploy \
    --domain taslug.org.au \
    --deploy-hook gitlab

export GITLAB_DOMAIN="www.taslug.org.au"
"$ACME_SH" --deploy \
    --domain taslug.org.au \
    --deploy-hook gitlab
