from os import path

AUTHOR = 'TasLUG'
SITENAME = 'TasLUG'
SITESUBTITLE = 'Tasmanian Linux Users Group'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Australia/Hobart'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

ARTICLE_URL = '{date:%Y}/{slug}.html'
ARTICLE_SAVE_AS = ARTICLE_URL
YEAR_ARCHIVE_URL = '{date:%Y}/index.html'
YEAR_ARCHIVE_SAVE_AS = YEAR_ARCHIVE_URL

DIRECT_TEMPLATES = [
    'index',
    'authors',
    'categories',
    'tags',
    'archives',
    '404',
]

# Blogroll
LINKS = None

# Social widget
SOCIAL = (
    ('Facebook', 'https://www.facebook.com/taslug'),
    ('Mailing List', 'http://lists.linux.org.au/mailman/listinfo/taslug'),
    ('IRC', 'ircs://irc.oftc.net:6697/taslug'),
    ('Matrix', 'https://matrix.to/#/#taslug:matrix.org'),
    ('GitLab', 'https://gitlab.com/taslug/'),
)
SOCIAL_ICONS = {
    'Facebook': 'facebook',
    'Mailing List': 'envelope',
    'IRC': 'hashtag',
    'Matrix': 'matrix-org',
    'GitLab': 'gitlab',
}

DEFAULT_PAGINATION = 10
PAGE_ORDER_BY = 'sortorder'
TYPOGRIFY = True
USE_FOLDER_AS_CATEGORY = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = path.join(path.dirname(__file__), 'themes', 'beautiful-taslug')
